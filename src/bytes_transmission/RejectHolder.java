package bytes_transmission;

/**
* bytes_transmission/RejectHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from orb_interface.idl
* dimanche 15 janvier 2017 22 h 03 CET
*/

public final class RejectHolder implements org.omg.CORBA.portable.Streamable
{
  public bytes_transmission.Reject value = null;

  public RejectHolder ()
  {
  }

  public RejectHolder (bytes_transmission.Reject initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = bytes_transmission.RejectHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    bytes_transmission.RejectHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return bytes_transmission.RejectHelper.type ();
  }

}
