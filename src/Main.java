import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.omg.CORBA.ORB;

import bytes_transmission.Dessine;
import bytes_transmission.DessineHelper;
import bytes_transmission.Image;
import bytes_transmission.Reject;

public class Main {

	private static final String imagePath = "/home/davide/flowers.png";
	private static String SERVER_IOR_FNAME = "/tmp/ior";
	private static Dessine DESSINE;

	public static void main(String[] args) {
		connect(args);

		Image img = null;
		try {
			img = loadFromFile(imagePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		Logger.getLogger(Main.class.getName()).log(Level.INFO, "Sending " + img.bytesCount + " bytes to server...");
		try {
			//DESSINE.sendImage(img);

			DESSINE.sendImageWithComments(img, new String[] { "bello ma brutto", "schifoso", "ammazzati" });
		} catch (Reject e) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE,
					"REQUEST REJECTED FROM THE SERVER. Reason: " + e.message);
			e.printStackTrace();
		} finally {
			Logger.getLogger(Main.class.getName()).log(Level.INFO, "DONE!");
		}

	}

	private static void connect(String[] args) {
		try {
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			String stringIOR;
			try (BufferedReader fileReader = new BufferedReader(new FileReader(SERVER_IOR_FNAME))) {
				stringIOR = fileReader.readLine();
			}

			// get the root naming context
			org.omg.CORBA.Object objRef = orb.string_to_object(stringIOR);
			DESSINE = DessineHelper.narrow(objRef);
		} catch (Exception e) {
			System.out.println("ERROR : " + e);
			e.printStackTrace(System.out);
		}
	}

	private static Image loadFromFile(String fname) throws IOException {
		Image img = new Image();
		File file = new File(fname);

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		BufferedImage bimg = ImageIO.read(new File(fname));
		img.width = bimg.getWidth();
		img.height = bimg.getHeight();
		bimg.flush();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		try {
			for (int readNum; (readNum = fis.read(buf)) != -1;) {
				// Writes to this byte array output stream
				bos.write(buf, 0, readNum);
			}
		} catch (IOException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}

		img.data = bos.toByteArray();
		img.bytesCount = img.data.length;

		return img;
	}
}
